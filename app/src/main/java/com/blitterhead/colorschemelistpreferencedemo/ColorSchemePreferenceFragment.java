package com.blitterhead.colorschemelistpreferencedemo;


import android.os.Bundle;
import android.preference.PreferenceFragment;
import androidx.fragment.app.Fragment;
import android.view.MenuItem;

/**
 * A simple {@link Fragment} subclass.
 */
public class ColorSchemePreferenceFragment extends PreferenceFragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		addPreferencesFromResource(R.xml.pref_colorscheme);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			getActivity().finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
