package com.blitterhead.colorschemelistpreference;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.os.Build;
import android.preference.ListPreference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

import androidx.annotation.RequiresApi;


/**
 * Extends the ListPreference to preview the currently selected color scheme. The list dialog that pops up also contains
 * a custom layout that previews each color scheme. It is tightly coupled to the Settings.GetFolderIndicatorColorSchemes()
 * which is used to retrieve the full list of color schemes.
 */
public class ColorSchemeListPreference extends ListPreference {
	//region Private Types
	/**
	 * A Folder Indicator ColorScheme has only two propeties: a name and a palette of colors. The colors are represented
	 * as an array of ints. There is no limit to the number of colors in the palette other than memory.
	 */
	public static class ColorScheme
	{
		public CharSequence name;
		public int[] colors;
	}


	/**
	 * A list adapter used to populate the listview in the preference dialog.
	 */
	private class ColorsSchemeListAdapter implements ListAdapter
	{
		private ColorScheme[] mColorSchemes;

		public ColorsSchemeListAdapter()
		{
			CharSequence[] colorSchemeNames = getEntries();

			if (mColorSchemeIdsArray.length > 0) {
				mColorSchemes = new ColorScheme[mColorSchemeIdsArray.length];

				for (int i = 0; i < mColorSchemeIdsArray.length; i++) {
					mColorSchemes[i] = new ColorScheme();
					mColorSchemes[i].name = colorSchemeNames[i];
					mColorSchemes[i].colors = getContext().getResources().getIntArray(mColorSchemeIdsArray[i]);
				}
			}
		}


		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater li = LayoutInflater.from(ColorSchemeListPreference.this.getContext());

				convertView = li.inflate(R.layout.select_dialog_singlechoice_colorscheme, parent, false);
			}

			CheckedTextView text1 = (CheckedTextView)convertView.findViewById(android.R.id.text1);

			text1.setText(mColorSchemes[position].name);
			text1.setChecked(0 == mColorSchemes[position].name.toString().compareTo(getValue()));

			LinearLayout colors_frame = (LinearLayout)convertView.findViewById(R.id.colors_frame);
			setColorScheme(colors_frame, mColorSchemes[position].colors);

			return convertView;
		}


		@Override
		public boolean areAllItemsEnabled() { return true; }

		@Override
		public boolean isEnabled(int position) { return true; }

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {}

		@Override
		public int getCount() { return mColorSchemes.length; }

		@Override
		public Object getItem(int position) { return mColorSchemes[position]; }

		@Override
		public long getItemId(int position ) { return position; }

		@Override
		public boolean hasStableIds() { return false; }

		@Override
		public int getItemViewType(int position) { return 0; }


		@Override
		public int getViewTypeCount() { return 1; }


		@Override
		public boolean isEmpty() { return mColorSchemes.length == 0; }
	}
	//endregion


	//region Private fields
	private ColorScheme mColorScheme;
	private int[] mColorSchemeIdsArray;
	private int mClickedDialogEntryIndex = -1;
	//endregion


	//region Public Static Methods
	/**
	 * Generates an array of ColorScheme, given two parallel arrays; one containing color scheme names and the other
	 * resource IDs for integer arrays. The two arrays much be parallel to each other, meaning, they must both be the same
	 * length and elements at each index are associated with that of the other array.
	 * @param resources The resources instance of the applications package.
	 * @param names An array of strings. Each element contains the display name for the color scheme
	 * @param ids As array of resource IDs. Each ID must correspond to an integer array which contains the color palette.
	 *           Each int is a 32 bit value that translates to a color in the following format: aarrggbb
	 * @return An array of ColorScheme.
	 * @throws RuntimeException If the two arrays are not the same length.
	 */
	public static ColorScheme[] loadColorSchemes(Resources resources, String[] names, int[] ids) {
		if (names.length != ids.length) {
			throw new RuntimeException("FolderIndictorColorSchemeNames length does not match FolderIndicatorColorSchemeIds length.");
		}

		ColorScheme[] colorSchemes = new ColorScheme[names.length];

		for (int i = 0; i < names.length; i++) {
			colorSchemes[i] = new ColorScheme();
			colorSchemes[i].name = names[i];
			colorSchemes[i].colors = resources.getIntArray(ids[i]);
		}

		return colorSchemes;
	}


	public static ColorScheme loadColorScheme(Resources resources, String[] names, int[] ids, CharSequence name)
	{
		ColorScheme colorScheme = null;

		for (int i = 0; i < names.length; i++) {
			if (TextUtils.equals(names[i], name)) {
				colorScheme = new ColorScheme();
				colorScheme.name = name;
				colorScheme.colors = resources.getIntArray(ids[i]);
			}
		}

		return colorScheme;
	}


	public static ColorScheme findColorScheme(ColorScheme[] colorSchemes, CharSequence name)
	{
		ColorScheme colorScheme = null;

		for (int i = 0; i < colorSchemes.length; i++) {
			if (TextUtils.equals(name, colorSchemes[i].name)) {
				colorScheme = colorSchemes[i];
			}
		}

		return colorScheme;
	}


	//region Constructors
	public ColorSchemeListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);

		initColorSchemeIdsArray(context, attrs);

		setWidgetLayoutResource(R.layout.color_scheme_view);
	}


	public ColorSchemeListPreference(Context context) {
		this(context, null);
	}


	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public ColorSchemeListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);

		initColorSchemeIdsArray(context, attrs);

		setWidgetLayoutResource(R.layout.color_scheme_view);
	}


	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public ColorSchemeListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
		this(context, attrs, defStyleAttr, 0);
	}
	//endregion


	private void initColorSchemeIdsArray(Context context, AttributeSet attrs)
	{
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ColorSchemeListPreference, 0, 0);
		int id = a.getResourceId(R.styleable.ColorSchemeListPreference_colorSchemeIdsArray, 0);
		a.recycle();

		TypedArray ta = context.getResources().obtainTypedArray(id);

		int length = ta.length();
		mColorSchemeIdsArray = new int[length];

		for (int i = 0; i < length; i++) {
			mColorSchemeIdsArray[i] = ta.getResourceId(i, 0);
		}

		ta.recycle();
	}


	@Override
	public View getView(View convertView, ViewGroup parent)
	{
		setSummary(getValue());

		View v = super.getView(convertView, parent);

		LinearLayout widget_frame = (LinearLayout)v.findViewById(android.R.id.widget_frame);
		LinearLayout colors_frame = (LinearLayout)widget_frame.findViewById(R.id.colors_frame);

		mColorScheme = loadColorScheme();

		if (mColorScheme != null) {
			setColorScheme(colors_frame, mColorScheme.colors);
		}

		return v;
	}


	// NOTE:
	// The framework forgot to call notifyChanged() in setValue() on previous versions of android.
	// This bug has been fixed in android-4.4_r0.7.
	// Commit: platform/frameworks/base/+/94c02a1a1a6d7e6900e5a459e9cc699b9510e5a2
	// Time: Tue Jul 23 14:43:37 2013 -0700
	//
	// However on previous versions, we have to workaround it by ourselves.
	@Override
	public void setValue(String value) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			super.setValue(value);
		} else {
			String oldValue = getValue();
			super.setValue(value);
			if (!TextUtils.equals(value, oldValue)) {
				notifyChanged();
			}
		}
	}

	private ColorScheme loadColorScheme()
	{
		String name = getValue().toString();
		CharSequence[] names = getEntryValues();
		ColorScheme colorScheme = new ColorScheme();

		for (int i = 0; i < names.length; i++) {
			if (0 == names[i].toString().compareTo(name))
			{
				colorScheme.name = name;
				colorScheme.colors = getContext().getResources().getIntArray(mColorSchemeIdsArray[i]);

				break;
			}
		}

		return colorScheme;
	}


	private void setColorScheme(LinearLayout colors_frame, int[] color_scheme)
	{
		final int colorFrameCount = colors_frame.getChildCount();

		for (int i = 0; i < colorFrameCount; i++)
		{
			View color_frame = colors_frame.getChildAt(i);

			if (i < color_scheme.length) {
				color_frame.setVisibility(View.VISIBLE);
				color_frame.setBackgroundColor(color_scheme[i]);
			}
			else {
				color_frame.setVisibility(View.GONE);
			}
		}
	}


	@Override
	protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
		super.onPrepareDialogBuilder(builder);

		CharSequence[] entries = getEntries();
		CharSequence[] entryValues = getEntryValues();

		if (entries == null || entryValues == null) {
			throw new IllegalStateException("ListPreference requires an entries array and an entryValues array.");
		}

		mClickedDialogEntryIndex = findIndexOfValue(getValue());

		builder.setSingleChoiceItems(new ColorsSchemeListAdapter(), mClickedDialogEntryIndex, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mClickedDialogEntryIndex = which;
				ColorSchemeListPreference.this.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
				dialog.dismiss();
			}
		});

        /*
         * The typical interaction for list-based dialogs is to have
         * click-on-an-item dismiss the dialog instead of the user having to
         * press 'Ok'.
         */
		builder.setPositiveButton(null, null);
	}


	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);

		CharSequence[] entryValues = getEntryValues();

		if (positiveResult && mClickedDialogEntryIndex >= 0 && entryValues != null) {
			String value = entryValues[mClickedDialogEntryIndex].toString();

			if (callChangeListener(value)) {
				setValue(value);
			}
		}
	}
}
